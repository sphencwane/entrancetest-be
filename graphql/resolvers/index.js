const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const Event = require('../../models/event');
const User = require('../../models/user');

const events = eventIds => {
    return Event.find({_id: {$in: eventIds}})
    .then(events => {
        return events.map(event => {
            return {...event._doc, date: new Date(event._doc.date).toISOString() , creator: user.bind(this, event.creator)}
        })
    })
    .catch(err => {
        throw err
    });
}

const user = userId => {
    return User.findById(userId)
    .then(user => {
        return {...user._doc, createdEvents: events.bind(this, user._doc.createdEvents)};
    })
    .catch(err =>{
        throw err;
    });
}


module.exports = {
    events: () => {
       return Event.find()
       .then(events => {
            return events.map(event => {
                return {... event._doc, date: new Date(event._doc.date).toISOString(), creator: user.bind(this, event._doc.creator)};
            });
        }).catch(err =>{
            throw err;
        }); 
    },
    bookings: async () => {
        try{
          const bookings = await Booking.find();
          return bookings.map(booking => {
              return {...booking._doc, createdAt: new Date(booking._doc.createdAt).toISOString(), updatedAt: new Date(booking._doc.updatedAt).toISOString()}
          })
        }catch(err){
            throw err;
        }
    },
    createEvent: (args, req) => {
        if(!req.isAuth){
            throw new Error('Unauthenticated!');
        }

       const event = new Event({
            title: args.eventInput.title,
            description: args.eventInput.description,
            price: args.eventInput.price,
            date: new Date(args.eventInput.date),
            creator: req.userId
       });
       let createdEvent;
      return event
       .save()
       .then(result => {
          createdEvent = {...result._doc, date: new Date(event._doc.date).toISOString(), creator: user.bind(this, result._doc.creator)};
          return User.findById(req.userId)
       })
       .then(user => {
        if(!user){
            throw new Error('User not found')
        }
        user.createdEvents.push(event);
        return user.save();
       })
       .then(result => {

        return createdEvent;
       })         
       .catch(err => {
           console.log(err);
           throw err;
       });
    },
    createUser: args => {  
      return User.findOne({
           email: args.userInput.email
       })
       .then(user =>{
            if(user){
                throw new Error('User exists.')
            }
            return bcrypt.hash(args.userInput.password, 12)
       })
       .then(hashedPassword => {
            const user = new User({
                email: args.userInput.email,
                password: hashedPassword
            });
            return user.save();
        })
        .then(result => {
            return {...result._doc};
        })
        .catch(err => {
            throw err;
        });
    }, 
    login: async ({email, password}) => {
        const user = await User.findOne({email: email});
        if(!user){
            throw new Error('User does not exist');
        }
        const isEqual = await bcrypt.compare(password, user.password);
        if(!isEqual){
            throw new Error('Password is incorrect!');
        }

        const token = await jwt.sign({userId: user.id, email: user.email}, 'somesupersecretkey',{
            expiresIn: '1h'
        });
        return {userId: user.id, token: token, tokenExpiration: 1};
    }
}